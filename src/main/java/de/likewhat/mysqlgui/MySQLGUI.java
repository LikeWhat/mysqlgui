package de.likewhat.mysqlgui;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.likewhat.mysqlgui.gui.Theme;
import de.likewhat.mysqlgui.mysql.*;
import de.likewhat.mysqlgui.util.JsonFile;
import de.likewhat.mysqlgui.util.Utils;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;
import java.util.stream.Collectors;

/*
 *  Project: MySQLGUI in MySQLGUI
 *     by LikeWhat
 *
 *  created on 24.02.2019 at 04:06
 */
public class MySQLGUI extends Application implements Initializable {

    public static final boolean DEBUG = true;
    private static final double VERSION = 1.4;
    private static JsonFile config;

    private final List<Theme> loadedThemes = new ArrayList<>();
    private static final Theme DEFAULT_THEME = new Theme("Light", "");
    private Theme activeTheme = DEFAULT_THEME;

    private double width = 1190;
    private double height = 725;

    private static final List<String> UPADTE_COMMANDS = Collections.unmodifiableList(Arrays.asList("USE", "CREATE", "DELETE", "INSERT", "UPDATE", "DROP"));
    private static final List<String> QUERY_COMMANDS = Collections.unmodifiableList(Arrays.asList("SELECT", "SHOW"));

    private MySQLConnector connector;
    private MySQLConnection connection;

    public Menu themeMenu;
    public Label schemaLabel;
    public Button submitButton;
    public TabPane resultTabPane;
    public TextArea inputField;
    public Accordion accordion;
    public AnchorPane mainPane;
    public TitledPane logPane;
    public ListView<Label> logList;
    public TreeView<String> schemas;

    private Scene mainScene;
    private Stage mainStage;

    private Image guiImage;
    private ImageView selectedImage;

    private TreeItem<String> selectedTreeItem;
    private TreeItem<String> schemaRootItem = new TreeItem<>();

    private MySQLSchema currentSchema;
    private List<MySQLSchema> availableSchemas = new ArrayList<>();

    public void start(Stage primaryStage) throws Exception {
        JsonFile.setDefaultSubfolder("data");
        config = new JsonFile("config.json");
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("menu.fxml"));
        primaryStage.setScene(new Scene(loader.load(), width, height));
        ((MySQLGUI)loader.getController()).initWithStage(primaryStage);
    }

    public void initWithStage(Stage stage) {
        JsonObject configJson = config.getJson().getAsJsonObject();
        JsonObject databaseConfig = configJson.getAsJsonObject("database-config");
        connector = new MySQLConnector(databaseConfig.get("username").getAsString(), databaseConfig.get("host").getAsString(), databaseConfig.has("database") ? databaseConfig.get("database").getAsString() : "", databaseConfig.has("port") ? databaseConfig.get("port").getAsInt() : 3306);

        System.out.print("Connecting to MySQL...");
        connection = connector.openConnection("", new MySQLConnectionResult() {
            public void success() {
                System.out.println("ok!");
            }

            public void error(Throwable error) {
                System.out.println("Failed to connect : " + error.getMessage());
                System.exit(1);
            }
        });

        // Load Themes
        ObservableList<MenuItem> menuItems = themeMenu.getItems();
        ToggleGroup toggleGroup = new ToggleGroup();
        ObservableList<Toggle> toggles = toggleGroup.getToggles();

        RadioMenuItem defaultThemeItem = new RadioMenuItem("Light");
        defaultThemeItem.setOnAction(a -> setActiveTheme(DEFAULT_THEME));
        toggles.add(defaultThemeItem);
        menuItems.add(defaultThemeItem);

        JsonArray themes = configJson.getAsJsonArray("themes");
        for(JsonElement themeRaw : themes) {
            Theme theme = Utils.GSON.fromJson(themeRaw, Theme.class);
            RadioMenuItem menuItem = new RadioMenuItem(theme.getName());
            menuItem.setOnAction(a -> setActiveTheme(theme));
            toggles.add(menuItem);
            menuItems.add(menuItem);
            if(theme.getName().equals(configJson.get("default-theme").getAsString())) {
                menuItem.setSelected(true);
                setActiveTheme(theme);
            }
            loadedThemes.add(theme);
        }

        // Init Main Stage and Scene
        mainStage = stage;
        mainScene = mainPane.getScene();

        currentSchema = connection.getDummySchema();

        selectedImage = new ImageView(new Image(getClass().getClassLoader().getResourceAsStream("selected-icon.png")));
        guiImage = new Image(getClass().getClassLoader().getResourceAsStream("gui-icon.png"));

        mainStage.getIcons().addAll(guiImage, guiImage);

        updateDatabaseList(false);

        // Connection MenuItem
        MenuItem connectToItem = new MenuItem("");
        connectToItem.setOnAction(actionEvent -> {
            TreeItem<String> treeItem = schemas.getSelectionModel().getSelectedItem();
            // Check if selected Tree Item is a Database
            if (treeItem != null && schemaRootItem.getChildren().contains(treeItem)) {
                String schemaName = treeItem.getValue();
                changeInputAvailable(false);
                // Update selected Database and connect to it
                connection.update("USE `" + schemaName + "`", new MySQLResult<Integer>() {
                    public void success(Integer integer) {
                        writeInfo("Connected to " + schemaName, false);
                        getSchemaByName(schemaName).ifPresent(schema -> setCurrentSchema(schema));
                        changeInputAvailable(true);
                    }

                    public void error(Throwable error) {
                        writeInfo("Failed to connect to Database: " + error.getMessage(), true);
                    }
                });
            }
        });
        MenuItem refreshItem = new MenuItem("Reload Schemas");
        refreshItem.setOnAction(actionEvent -> updateDatabaseList(true));
        MenuItem createTableItem = new MenuItem("Create Table");
        createTableItem.setOnAction(actionEvent -> {
            // Soon™
        });
        ContextMenu contextMenu = new ContextMenu(connectToItem, refreshItem);
        schemas.setContextMenu(contextMenu);

        // Change Context Menu Items depending on which Item is selected
        schemas.setOnContextMenuRequested(contextEvent -> {
            TreeItem<String> treeItem = schemas.getSelectionModel().getSelectedItem();
            MenuItem item = contextMenu.getItems().get(0);
            if (treeItem != null && schemaRootItem.getChildren().contains(treeItem)) {
                item.setDisable(false);
                item.setText("Connect to " + treeItem.getValue());
            } else {
                item.setDisable(true);
                item.setText("Not available (Connect)");
            }
            contextMenu.getItems().set(0, item);
        });

        schemas.setRoot(schemaRootItem);

        accordion.setExpandedPane(accordion.getPanes().get(0));

        stage.setResizable(false);
        stage.setTitle("MySQL GUI v" + VERSION);

        // Disconnect from MySQL before quitting
        stage.setOnCloseRequest(closeRequest -> close());
        stage.show();
    }

    public void initialize(URL location, ResourceBundle resources) {}

    public void openSQLFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MySQL Files", "*.sql"));
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileChooser.showOpenDialog(mainScene.getWindow()))))){
            StringBuilder builder = new StringBuilder();
            reader.lines().forEach(string -> builder.append(string).append("\n"));
            inputField.setText(builder.toString());
        } catch (NullPointerException ignored) {
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void close(ActionEvent actionEvent) {
        close();
    }

    public void runAll(ActionEvent actionEvent) {
        executeSQL(inputField.getText().split(";"));
    }

    public void runSelected(ActionEvent actionEvent) {
        executeSQL(inputField.getSelectedText().split(";"));
    }

    public void runAtCursor(ActionEvent actionEvent) {
        String fullText = inputField.getText();
        String[] queriesSplitted = fullText.split(";");
        int splitQueryIndex = 0;
        int queryIndex = 0;
        for (String queryRaw : queriesSplitted) {
            splitQueryIndex += queryRaw.length();
            if (splitQueryIndex >= inputField.getSelection().getStart()) {
                break;
            }
            queryIndex++;
        }
        executeSQL(queriesSplitted[queryIndex]);
    }

    public void submitAction(ActionEvent actionEvent) {
        String[] queries = (inputField.getSelectedText().isEmpty() ? inputField.getText() : inputField.getSelectedText()).split(";");
        executeSQL(queries);
    }

    private void close() {
        connection.closeConnection(new MySQLConnectionResult() {
            public void success() {
                System.out.println("Successfully disconnected from MySQL");
                System.exit(0);
            }

            public void error(Throwable error) {
                error.printStackTrace();
            }
        });
    }

    private void executeSQL(String... queries) {
        if(queries.length == 0) {
            return;
        }
        resultTabPane.getTabs().removeAll(resultTabPane.getTabs().filtered(tab -> !tab.getText().equals("Info")));
        for (int i = 0; i < queries.length; i++) {
            String query = queries[i];
            StringBuilder cleanQuery = new StringBuilder();
            for(String a : query.split("\n")) {
                if(!(a.startsWith("--") || a.isEmpty())) {
                    if(cleanQuery.length() != 0 && !cleanQuery.substring(cleanQuery.length() - 1).isEmpty())
                        cleanQuery.append(" ");
                    cleanQuery.append(a);
                }
            }
            query = cleanQuery.toString();
            if(query.isEmpty())
                continue;
            String resultName = "Result " + (i + 1);
            String finalQuery = query;
            if (QUERY_COMMANDS.stream().anyMatch(command -> finalQuery.toUpperCase().startsWith(command))) {
                connection.query(query, new MySQLResult<ResultSet>() {
                    public void success(ResultSet resultSet) {
                        try {
                            TableView<String[]> tableView = new TableView<>();
                            AnchorPane anchorPane = new AnchorPane(tableView);
                            Tab tab = new Tab(resultName, anchorPane);
                            resultTabPane.getTabs().add(tab);
                            tableView.setPrefSize(resultTabPane.getWidth(), resultTabPane.getHeight()-25);

                            List<String[]> entries = new ArrayList<>();
                            ResultSetMetaData metaData = resultSet.getMetaData();

                            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                                TableColumn<String[], String> tableColumn = new TableColumn<>(metaData.getColumnName(i));
                                int finalI = i;
                                tableColumn.setCellValueFactory(cd -> {
                                    String value = cd.getValue()[finalI - 1];
                                    return new SimpleStringProperty(value == null ? "null" : value);
                                });
                                tableView.getColumns().add(tableColumn);
                            }

                            while (resultSet.next()) {
                                String[] row = new String[0];
                                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                                    row = Utils.appendArray(row, resultSet.getString(i));
                                }
                                entries.add(row);
                            }
                            tableView.getItems().addAll(entries);

                            writeInfo(resultName + ": " + entries.size() + " Row(s) returned", false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    public void error(Throwable error) {
                        writeInfo("Failed to execute Query: " + error.getMessage(), true);
                    }
                });
            } else {
                connection.update(query, new MySQLResult<Integer>() {
                    public void success(Integer integer) {
                        if (finalQuery.toLowerCase().startsWith("use")) {
                            getSchemaByName(finalQuery.split(" ")[1]).ifPresent(MySQLGUI.this::setCurrentSchema);
                        }
                        writeInfo(resultName + ": " + integer, false);
                    }

                    public void error(Throwable error) {
                        writeInfo("Failed to execute Update: " + error.getMessage(), true);
                    }
                });
            }
            resultTabPane.getSelectionModel().select(resultTabPane.getTabs().size() - 1);
        }
    }

    // Write given text under Input Field (Can be Red when defined as Error)
    private void writeInfo(String text, boolean error) {
        Label label = new Label(text);
        if(error) {
            label.setTextFill(Color.RED);
            if(!accordion.getExpandedPane().equals(logPane)) {
                accordion.setExpandedPane(logPane);
            }
        }
        logList.getItems().add(label);

    }

    // Update Database Catalog
    private void updateDatabaseList(boolean reconnect) {
        // Clear Schemas
        setSchemaName("None");
        availableSchemas.clear();
        schemaRootItem.getChildren().clear();
        schemaRootItem.setExpanded(false);
        schemaRootItem.setValue("Fetching Databases...");

        if (reconnect) {
            // Reconnect Database
            connection.closeConnection();
            connection = connector.openConnection("", new MySQLConnectionResult() {
                public void error(Throwable error) {
                    writeInfo("Failed to reconnect to Database: " + error.getMessage(), true);
                }
            });
        }

        // Fetch Databases from MySQL
        connection.getDatabases(new MySQLResult<List<MySQLSchema>>() {
            public void success(List<MySQLSchema> mySQLSchema) {
                // Add Databases back into the Root Item
                mySQLSchema.forEach(database -> {
                    TreeItem<String> databaseItem = new TreeItem<>();
                    databaseItem.setValue(database.getName());
                    databaseItem.getChildren().addAll(database.getTables().stream().map(table -> {
                        TreeItem<String> col = new TreeItem<>(table.getName());
                        col.getChildren().addAll(table.getColumns().stream().map(column -> new TreeItem<>(column[0] + " : " + column[2])).collect(Collectors.toList()));
                        return col;
                    }).collect(Collectors.toList()));
                    schemaRootItem.getChildren().add(databaseItem);
                    availableSchemas.add(database);
                });
                schemaRootItem.setValue(mySQLSchema.size() + " Schema(s) found");
                schemaRootItem.setExpanded(true);

                getSchemaByName(currentSchema.getName()).ifPresent(schema -> {
                    if (!currentSchema.getName().equals("None")) {
                        connection.update("USE `" + currentSchema.getName() + "`", new MySQLResult<Integer>() {
                            public void success(Integer integer) {
                                Platform.runLater(() -> setCurrentSchema(schema));
                            }

                            public void error(Throwable error) {
                                writeInfo("Failed to reconnect to Schema: " + error.getMessage(), true);
                            }
                        });
                    }
                });
            }

            public void error(Throwable error) {
                schemaRootItem.setValue("Failed to Fetch Schemas");
            }
        });
    }

    private void changeInputAvailable(boolean available) {
        submitButton.setDisable(!available);
        inputField.setDisable(!available);
    }

    private void setCurrentSchema(MySQLSchema currentSchema) {
        this.currentSchema = currentSchema;
        if (selectedTreeItem != null) {
            selectedTreeItem.setGraphic(null);
        }
        Optional<TreeItem<String>> foundTreeItem = schemaRootItem.getChildren().stream().filter(treeItem -> treeItem.getValue().equalsIgnoreCase(currentSchema.getName())).findFirst();
        foundTreeItem.ifPresent(item -> {
            selectedTreeItem = item;
            item.setGraphic(selectedImage);
        });
        setSchemaName(currentSchema.getName());
    }

    private void setSchemaName(String name) {
        schemaLabel.setText("Schema: " + name);
    }

    private Optional<MySQLSchema> getSchemaByName(String schemaName) {
        return availableSchemas.stream().filter(schema -> schema.getName().equals(schemaName)).findFirst();
    }

    private void setActiveTheme(Theme theme) {
        if(activeTheme != theme) {
            mainPane.setStyle((activeTheme = theme).getCss());
        }
    }

}
