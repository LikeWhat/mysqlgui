package de.likewhat.mysqlgui.mysql;

import java.util.List;

/*
 *  Project: MySQLGUI in MySQLSchema
 *     by LikeWhat
 *
 *  created on 08.02.2019 at 12:16
 */
public interface MySQLSchema {

    String getName();

    List<MySQLTable> getTables();

}
