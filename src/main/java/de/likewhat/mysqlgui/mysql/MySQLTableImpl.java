package de.likewhat.mysqlgui.mysql;

import java.util.List;

/*
 *  Project: MySQLGUI in MySQLTableImpl
 *     by LikeWhat
 *
 *  created on 10.02.2019 at 21:42
 */
public class MySQLTableImpl implements MySQLTable {

    private String name;
    private List<String[]> columns;

    protected MySQLTableImpl(String name, List<String[]> columns) {
        this.name = name;
        this.columns = columns;
    }

    public String getName() {
        return name;
    }

    public List<String[]> getColumns() {
        return columns;
    }
}
