package de.likewhat.mysqlgui.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/*
 *  Project: Aufgaben in MySQLConnectionImpl
 *     by LikeWhat
 *
 *  created on 18.01.2019 at 10:43
 */
public class MySQLConnectionImpl implements MySQLConnection {

    private Connection connection;

    protected MySQLConnectionImpl(Connection connection) {
        try {
            if (connection.isClosed()) {
                throw new IllegalStateException("Connection already closed");
            }
            this.connection = connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void query(String sql, MySQLResult<ResultSet> result) {
        new Thread(() -> {
            try {
                result.success(connection.prepareStatement(sql).executeQuery());
            } catch (SQLException e) {
                result.error(e);
            }
        }).run();
    }

    @Override
    public void update(String sql, MySQLResult<Integer> result) {
        new Thread(() -> {
            try {
                result.success(connection.prepareStatement(sql).executeUpdate());
            } catch (SQLException e) {
                result.error(e);
            }
        }).run();
    }

    @Override
    public void closeConnection() {
        closeConnection(new MySQLConnectionResult() {});
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public MySQLSchema getDummySchema() {
        return new MySQLSchemaImpl("None", new ArrayList<>());
    }

    @Override
    public void getDatabases(MySQLResult<List<MySQLSchema>> result) {
        new Thread(() -> query("SHOW DATABASES", new MySQLResult<ResultSet>() {
            public void success(ResultSet rs) {
                try {
                    HashMap<String, List<MySQLTable>> databasesRaw = new HashMap<>();
                    while (rs.next()) {
                        String databaseName = rs.getString(1);
                        if(databaseName.equals("information_schema") || databaseName.equals("performance_schema"))
                            continue;
                        getDatabaseTables(databaseName, new MySQLResult<List<MySQLTable>>() {
                            public void success(List<MySQLTable> tables) {
                                databasesRaw.put(databaseName, tables);
                            }
                        });
                    }
                    result.success(databasesRaw.entrySet().stream().map(entry -> new MySQLSchemaImpl(entry.getKey(), entry.getValue())).collect(Collectors.toList()));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    result.error(e);
                }
            }
        })).start();
    }

    private void getDatabaseTables(String database, MySQLResult<List<MySQLTable>> result) {
        query("SELECT * FROM information_schema.columns WHERE table_schema = '" + database + "'", new MySQLResult<ResultSet>() {
            public void success(ResultSet resultSet) {
                try {
                    HashMap<String, List<String[]>> tablesRaw = new HashMap<>();
                    while (resultSet.next()) {
                        String tablename = resultSet.getString("TABLE_NAME");
                        List<String[]> columns;
                        if(tablesRaw.containsKey(tablename)) {
                            columns = tablesRaw.get(tablename);
                        } else {
                            columns = new ArrayList<>();
                        }
                        columns.add(new String[] { resultSet.getString("COLUMN_NAME"), resultSet.getString("DATA_TYPE"), resultSet.getString("COLUMN_TYPE")});
                        tablesRaw.put(tablename, columns);
                    }
                    result.success(tablesRaw.entrySet().stream().map(entry -> new MySQLTableImpl(entry.getKey(), entry.getValue())).collect(Collectors.toList()));
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }

            public void error(Throwable error) {
                error.printStackTrace();
            }
        });
    }

    @Override
    public void getCurrentDatabase(MySQLResult<Optional<MySQLSchema>> result) {
        query("SELECT DATABASE()", new MySQLResult<ResultSet>() {
            public void success(ResultSet resultSet) {
                try {
                    while (resultSet.next()) {
                        String databaseName = resultSet.getString(1);
                        getDatabaseTables(databaseName, new MySQLResult<List<MySQLTable>>() {
                            public void success(List<MySQLTable> tables) {
                                result.success(databaseName == null ? Optional.empty() : Optional.of(new MySQLSchemaImpl(databaseName, tables)));
                            }
                        });
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }

            public void error(Throwable error) {
                error.printStackTrace();
            }
        });
    }

    @Override
    public void closeConnection(MySQLConnectionResult result) {
        try {
            if (connection.isClosed()) {
                throw new IllegalStateException("Connection already closed");
            }
            connection.close();
            result.success();
        } catch (SQLException e) {
            result.error(e);
        }
    }
}
