package de.likewhat.mysqlgui.mysql;

import java.util.List;

/*
 *  Project: MySQLGUI in MySQLSchemaImpl
 *     by LikeWhat
 *
 *  created on 08.02.2019 at 12:17
 */
public class MySQLSchemaImpl implements MySQLSchema {

    private String name;
    private List<MySQLTable> tables;

    protected MySQLSchemaImpl(String name, List<MySQLTable> tables) {
        this.name = name;
        this.tables = tables;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<MySQLTable> getTables() {
        return tables;
    }
}

