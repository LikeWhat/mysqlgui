package de.likewhat.mysqlgui.mysql;

import java.util.List;

/*
 *  Project: MySQLGUI in MySQLTable
 *     by LikeWhat
 *
 *  created on 10.02.2019 at 21:42
 */
public interface MySQLTable {

    String getName();

    List<String[]> getColumns();

}
