package de.likewhat.mysqlgui.mysql;

/*
 *  Project: MySQLGUI in MySQLResult
 *     by LikeWhat
 *
 *  created on 07.02.2019 at 16:29
 */

public interface MySQLResult<T> {

    default void success(T t) {}

    default void error(Throwable error) {}

}
