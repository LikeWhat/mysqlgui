package de.likewhat.mysqlgui.mysql;

/*
 *  Project: MySQLGUI in MySQLConnectionResult
 *     by LikeWhat
 *
 *  created on 07.02.2019 at 15:46
 */
public interface MySQLConnectionResult {

    default void success() {}

    default void error(Throwable error) {}

}
