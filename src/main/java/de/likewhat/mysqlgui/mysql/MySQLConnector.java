package de.likewhat.mysqlgui.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 *  Project: Aufgaben in MySQLConnector
 *     by LikeWhat
 *
 *  created on 18.01.2019 at 10:20
 */
public class MySQLConnector {

    private static final String CONNECTION_URL = "jdbc:mysql://%s:%s/%s?useLegacyDatetimeCode=false&serverTimezone=Europe/Amsterdam";

    private String username;
    private String hostname;
    private String databaseName;
    private int port;

    public MySQLConnector(String username, String hostname, String databaseName, int port) {
        this.username = username;
        this.hostname = hostname;
        this.databaseName = databaseName;
        if(port < 0 || port > 65535) {
            throw new IllegalArgumentException("Invalid Port: " + port + " expected Port between 0 and 65535");
        }
        this.port = port;
    }

    public MySQLConnection openConnection(String password, MySQLConnectionResult result) {
        try {
            Connection connection = DriverManager.getConnection(String.format(CONNECTION_URL, hostname, port, databaseName), username, password);
            result.success();
            return new MySQLConnectionImpl(connection);
        } catch(SQLException e) {
            result.error(e);
        }
        return null;
    }

    public MySQLConnection openConnection(String password) {
        return openConnection(password, new MySQLConnectionResult() {});
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }
}
