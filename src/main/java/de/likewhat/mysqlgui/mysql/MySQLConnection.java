package de.likewhat.mysqlgui.mysql;/*
 *  Project: Aufgaben in MySQLConnection
 *     by LikeWhat
 *
 *  created on 18.01.2019 at 10:21
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

public interface MySQLConnection {

    void query(String sql, MySQLResult<ResultSet> result);

    void update(String sql, MySQLResult<Integer> result);

    void closeConnection();

    void closeConnection(MySQLConnectionResult result);

    void getDatabases(MySQLResult<List<MySQLSchema>> result);

    void getCurrentDatabase(MySQLResult<Optional<MySQLSchema>> result);

    MySQLSchema getDummySchema();

    Connection getConnection();
}
