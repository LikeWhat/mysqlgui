package de.likewhat.mysqlgui.util;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.likewhat.mysqlgui.MySQLGUI;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.channels.Channels;
import java.util.Arrays;
import java.util.Random;

public class Utils {

    public static final Gson GSON_PRETTY = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
    public static final Gson GSON = new GsonBuilder().disableHtmlEscaping().create();

    public static File saveInternalFile(String filename, String... sub) {
        try {
            String subfolder = sub.length > 0 ? sub[0] : "";
            File outFile = new File(subfolder, filename);
            Files.createParentDirs(outFile);
            if (!outFile.exists()) {
                try (InputStream fileInputStream = MySQLGUI.class.getClassLoader().getResourceAsStream(filename); FileOutputStream fileOutputStream = new FileOutputStream(outFile)) {
                    fileOutputStream.getChannel().transferFrom(Channels.newChannel(fileInputStream), 0, Integer.MAX_VALUE);
                } catch (FileNotFoundException e) {
                    System.out.println("Failed to create File " + filename + "\n" + e);
                }
            }
            return outFile;
        } catch (IOException e) {
            System.out.println("Failed to create File " + filename + "\n" + e);
        }
        return null;
    }

    public static int randomBetween(int start, int end) {
        return new Random().nextInt(end - start) + start;
    }

    public static double[] appendArray(double[] array, double what) {
        array = Arrays.copyOf(array, array.length + 1);
        array[array.length - 1] = what;
        return array;
    }

    public static <T> T[] appendArray(T[] array, T what) {
        array = Arrays.copyOf(array, array.length + 1);
        array[array.length - 1] = what;
        return array;
    }

    public static double round(double value, int places) {
        return new BigDecimal(value).setScale(places, RoundingMode.HALF_UP).doubleValue();
    }

}
