package de.likewhat.mysqlgui.util;


import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static de.likewhat.mysqlgui.util.Utils.saveInternalFile;

/*
 *  Project: MySQLGUI in JsonFile
 *     by LikeWhat
 *
 *  created on 25.02.2019 at 14:49
 */
public class JsonFile {

    private static final JsonParser PARSER = new JsonParser();

    private static String defaultSubfolder = "";

    private JsonElement json;

    private File file;

    public JsonFile(String filename) {
        this(filename, defaultSubfolder);
    }

    public JsonFile(File file) {
        this.file = file;
        reload();
    }

    public JsonFile(String filename, String subFolder) {
        file = saveInternalFile(filename, subFolder);
        reload();
    }

    public void saveJson() {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            String[] jsonSplitted = Utils.GSON_PRETTY.toJson(json).split("\n");
            for (String line : jsonSplitted) {
                writer.write(line + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reload() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            json = PARSER.parse(reader);
        } catch (Exception e) {
            System.out.println("Failed to read Json from " + file.getName() + "\n" + e);
        }
    }

    public String getJsonAsString() {
        return json.toString();
    }

    public void setJson(JsonElement json) {
        this.json = json;
    }

    public static String getDefaultSubfolder() {
        return defaultSubfolder;
    }

    public static void setDefaultSubfolder(String defaultSubfolder) {
        JsonFile.defaultSubfolder = defaultSubfolder;
    }

    public JsonElement getJson() {
        return json;
    }

    public File getFile() {
        return file;
    }
}
