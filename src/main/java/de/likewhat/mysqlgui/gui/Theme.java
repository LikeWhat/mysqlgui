package de.likewhat.mysqlgui.gui;

/*
 *  Project: MySQLGUI in Theme
 *     by LikeWhat
 *
 *  created on 27.02.2019 at 22:46
 */
public class Theme {

    private String name;
    private String css;

    public Theme(String name, String css) {
        this.name = name;
        this.css = css;
    }

    public String getName() {
        return name;
    }

    public String getCss() {
        return css;
    }
}
