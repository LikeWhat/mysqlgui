# Changelog

###1.4
+ Added Themes

###1.3
+ Changed to FXML Loader

### 1.2
+ Added Sideview for all available Schemas

### 1.1
+ Added most of the MySQL Connection Code (e.g. MySQLTable, MySQLSchema)

### 1.0
+ Added Main Support for MySQL (Result Table, SQL Input)